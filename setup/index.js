const mainContainer = document.querySelector(".preview");
const cardContainer = document.querySelector(".layout-placeholder");
const spaceBetweenFilter = document.querySelector("#cardSpaceBetween");
const dynamicColumnsSelector = document.querySelector("#numberOfColumns");
const themeInputs = document.querySelectorAll(".theme-inputs");
const socialFilterGroup = document.querySelectorAll(".social-filter");
const cardBackgroundColor = document.querySelector("#cardBackgroundColor");
const mediaQueryLaptop = window.matchMedia("(min-width: 992px)");
const mediaQueryTablet = window.matchMedia("(min-width: 768px)");
const mediaQueryMobile = window.matchMedia("(max-width: 767px)");

const validHexArr = [
  "#",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
];
const button = document.createElement("button");
button.textContent = "LOAD MORE";
button.classList.add("btn-load");
mainContainer.append(button);
let bgColor = "#ffffff";
let socialFilter = "all";
let numOfObjects = 0;
let dataLength;

const card = (item, spaceBetween, dynamicCols, theme, backgroundColor) => {
  
  let { likes, show_more } = item;
  const {
    image,
    caption,
    type,
    source_type,
    source_link,
    date,
    name,
    profile_image,
  } = item;
  const slicedText = caption.split(" ").slice(1, caption.length).join(" ");
  const dateDisplayStyle = new Date(date).toDateString().split(" ");

  const cardWrapper = document.createElement("div");
  const cardInnerWrapper = document.createElement("div");
  const cardTopSection = document.createElement("div");
  const cardTopMidSection = document.createElement("div");
  const cardTitle = document.createElement("h3");
  const cardDate = document.createElement("p");
  const cardProfileImg = document.createElement("img");
  const cardSocialLogo = document.createElement("img");
  const cardFigureContainer = document.createElement("figure");
  const cardMainImg = document.createElement("img");
  const cardCaptionContainer = document.createElement("figcaption");
  const cardAncor = document.createElement("a");
  const cardText = document.createElement("span");
  const cardBottomSection = document.createElement("div");
  const cardFavouriteCounter = document.createElement("p");
  const showMoreText = document.createElement("span");
  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");

  const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  path.setAttribute(
    "d",
    "M14.7617 3.26543C14.3999 2.90347 13.9703 2.61634 13.4976 2.42045C13.0248 2.22455 12.518 2.12372 12.0063 2.12372C11.4945 2.12372 10.9878 2.22455 10.515 2.42045C10.0422 2.61634 9.61263 2.90347 9.25085 3.26543L8.50001 4.01626L7.74918 3.26543C7.0184 2.53465 6.02725 2.1241 4.99376 2.1241C3.96028 2.1241 2.96913 2.53465 2.23835 3.26543C1.50756 3.99621 1.09702 4.98736 1.09702 6.02084C1.09702 7.05433 1.50756 8.04548 2.23835 8.77626L2.98918 9.52709L8.50001 15.0379L14.0108 9.52709L14.7617 8.77626C15.1236 8.41448 15.4108 7.98492 15.6067 7.51214C15.8026 7.03935 15.9034 6.53261 15.9034 6.02084C15.9034 5.50908 15.8026 5.00233 15.6067 4.52955C15.4108 4.05677 15.1236 3.62721 14.7617 3.26543V3.26543Z"
  );
  svg.setAttribute("width", "19");
  svg.setAttribute("height", "19");
  svg.setAttribute("viewBox", "0 0 17 17");
  svg.setAttribute("fill", "none");
  path.setAttribute("stroke", "black");
  path.setAttribute("stroke-linecap", "round");
  path.setAttribute("stroke-linejoin", "round");

  cardAncor.href = source_link;
  cardAncor.target = "_blank";
  cardMainImg.src = image;
  cardProfileImg.src = profile_image;
  cardSocialLogo.src = `../icons/${source_type}.svg`;
  cardWrapper.style.flexBasis =
    dynamicColumnsSelector.value === "dynamic"
      ? handleResize()
      : `${(100 / parseInt(dynamicCols)).toLocaleString()}%`;

  cardCaptionContainer.classList.add("margin-bottom-20");
  cardBottomSection.classList.add("flex");
  cardFavouriteCounter.classList.add("margin-bottom-0", "ml");
  cardWrapper.classList.add("layout-inner");
  cardInnerWrapper.classList.add("card-inner-wrapper");
  cardInnerWrapper.classList.add(theme ? "dark-theme" : "light-theme");
  if (cardInnerWrapper.classList.contains("light-theme")) {
    cardInnerWrapper.style.backgroundColor = backgroundColor;
  }
  cardInnerWrapper.style.marginRight = spaceBetween || "10px";
  cardTopSection.classList.add("card-top-section");
  cardTopMidSection.classList.add("ml");
  cardProfileImg.classList.add("profile-image");
  cardSocialLogo.classList.add("social-icon");
  cardMainImg.classList.add("card-main-img");
  cardDate.classList.add("margin-bottom-0");
  showMoreText.classList.add("show-more");

  cardAncor.textContent = `#${caption.split(" ")[0]} `;
  cardText.textContent = !show_more ? slicedText.slice(0, 120) : slicedText;
  cardTitle.textContent = name;
  cardDate.textContent = `${dateDisplayStyle[2]} ${dateDisplayStyle[1]} ${dateDisplayStyle[3]}`;
  cardFavouriteCounter.textContent = likes;
  showMoreText.textContent = "...show more";

  cardBottomSection.append(svg, cardFavouriteCounter);
  cardTopMidSection.append(cardTitle, cardDate);
  cardCaptionContainer.append(cardAncor, cardText, showMoreText);
  svg.appendChild(path);
  cardTopSection.append(cardProfileImg, cardTopMidSection, cardSocialLogo);
  cardFigureContainer.append(cardMainImg, cardCaptionContainer);
  cardInnerWrapper.append(
    cardTopSection,
    cardFigureContainer,
    cardBottomSection
  );
  cardWrapper.append(cardInnerWrapper);

  showMoreText.addEventListener("click", (e) => {
     e.target.textContent = show_more ? "...show more" : "...show less"
    show_more = !show_more;
    cardText.textContent = !show_more ? slicedText.slice(0, 120) : slicedText;
  });

  path.addEventListener("click", (e) => {
    e.stopPropagation();
    e.currentTarget.parentElement.classList.contains("bg-red")
      ? (e.currentTarget.parentElement.classList.remove("bg-red"),
        (likes = parseInt(likes) - 1))
      : (e.currentTarget.parentElement.classList.add("bg-red"),
        (likes = parseInt(likes) + 1));
    cardFavouriteCounter.textContent = likes;
  });
  svg.addEventListener("click", (e) => {
    e.target.classList.contains("bg-red")
      ? (e.target.classList.remove("bg-red"), (likes = parseInt(likes) - 1))
      : (e.target.classList.add("bg-red"), (likes = parseInt(likes) + 1));
    cardFavouriteCounter.textContent = likes;
  });

  return cardWrapper;
};

const handleResize = () => {
  switch (true) {
    case mediaQueryLaptop.matches:
      return "33.333%";
    case mediaQueryTablet.matches:
      return "50%";
    case mediaQueryMobile.matches:
      return "100%";
    default:
      "33.333%";
      break;
  }
};

const fetchData = (num) => {
  fetch("../data.json")
    .then((res) => res.json())
    .then((data) => {
      const filteredData = data.filter(
        (element) => element.source_type === socialFilter
      );
      const displayContent = (arr) => {
        arr
          .slice(num, num + 4)
          .forEach((item) =>
            cardContainer.appendChild(
              card(
                item,
                spaceBetweenFilter.value,
                dynamicColumnsSelector.value,
                themeInputs[1].checked,
                bgColor
              )
            )
          );
        dataLength = arr.length;
      };

      filteredData.length > 0
        ? displayContent(filteredData)
        : displayContent(data);
    });
};

button.addEventListener("click", function () {
  if (dataLength <= numOfObjects + 8) {
    this.style.visibility = "hidden";
  }
  numOfObjects += 4;
  fetchData(numOfObjects);
});

dynamicColumnsSelector.addEventListener("change", () => {
  cardContainer.innerHTML = "";
  fetchData(numOfObjects);
});
spaceBetweenFilter.addEventListener("keyup", (e) => {
  if (e.target.value.length >= 4) {
    setTimeout(() => {
      cardContainer.innerHTML = "";
      fetchData(numOfObjects);
    }, 1000);
  }
});
themeInputs.forEach((input) =>
  input.addEventListener("change", (e) => {
    bgColor = "#ffffff";
    cardBackgroundColor.value = bgColor;
    cardContainer.innerHTML = "";
    fetchData(numOfObjects);
  })
);
socialFilterGroup.forEach((input) =>
  input.addEventListener("change", (e) => {
    socialFilter = e.target.value;
    cardContainer.innerHTML = "";
    numOfObjects = 0;
    fetchData(numOfObjects);
  })
);
cardBackgroundColor.addEventListener("keyup", (e) => {
  if (
    e.target.value.length >= 4 &&
    e.target.value.length <= 7 &&
    e.target.value.split("").every((letter) => validHexArr.includes(letter)) &&
    e.target.value.startsWith("#", 0)
  ) {
    bgColor = e.target.value;
    cardContainer.innerHTML = "";
    fetchData(numOfObjects);
  }
});

window.addEventListener("load", () => fetchData(numOfObjects));
